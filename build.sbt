val ScalatraVersion = "2.6.4"

organization := "org.bitbucket.honstain"

name := "DoubleRecordInventoryService"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.6"

resolvers += Classpaths.typesafeReleases

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-json" % ScalatraVersion,
  "org.json4s"   %% "json4s-jackson" % "3.5.2",

  "com.typesafe" % "config" % "1.3.2",

  "com.typesafe.slick" %% "slick" % "3.3.0",
  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.0",
  "org.postgresql" % "postgresql" % "42.2.5", // org.postgresql.ds.PGSimpleDataSource dependency

  "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",

  "net.logstash.logback" % "logstash-logback-encoder" % "5.0" % "runtime",
  "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",

  "org.eclipse.jetty" % "jetty-webapp" % "9.4.9.v20180320" % "compile;container",

  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",

  "com.outworkers" %% "phantom-dsl" % "2.41.0",
  "org.scala-lang" % "scala-reflect" % scalaVersion.value,
)

enablePlugins(JavaAppPackaging)
enablePlugins(ScalatraPlugin)
