package org.bitbucket.honstain.inventory.dao

import org.bitbucket.honstain.PostgresSpec
import org.postgresql.util.PSQLException
import org.scalatest.BeforeAndAfter
import org.scalatra.test.scalatest._
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class InventorySingleRecordDaoTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  def createInventoryTable: DBIO[Int] =
    sqlu"""
          CREATE TABLE inventory_single
          (
            id bigserial NOT NULL,
            sku text,
            qty integer,
            location text,
            CONSTRAINT pk_single PRIMARY KEY (id),
            UNIQUE (sku, location)
          );
      """
  def dropInventoryTable: DBIO[Int] =
    sqlu"""
          DROP TABLE IF EXISTS inventory_single;
      """

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  val TEST_SKU = "NewSku"
  val BIN_01 = "Bin-01"
  val BIN_02 = "Bin-02"

  test("findAll empty") {
    val futureFind = InventorySingleRecordDao.findAll(database)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)
    findResult should equal(List())
  }

  test("findAll") {
    createInventoryHelper(TEST_SKU, 1, BIN_01)
    createInventoryHelper(TEST_SKU, 1, BIN_02)
    createInventoryHelper(TEST_SKU, 1, BIN_01)
    createInventoryHelper(TEST_SKU, 1, BIN_01)

    val futureFind = InventorySingleRecordDao.findAll(database)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should contain only (
      InventorySingleRecord(Some(1), TEST_SKU, 1, BIN_01),
      InventorySingleRecord(Some(2), TEST_SKU, 1, BIN_02),
    )
  }

  test("findBySku for nothing found") {
    val future = InventorySingleRecordDao.findBySku(database, "NotFoundSku")
    val result: Seq[InventorySingleRecord] = Await.result(future, Duration.Inf)

    result should equal(List())
  }

  test("findBySku for single SKU") {
    val createdBin0 = createInventoryHelper(TEST_SKU, 3, BIN_01)
    val createdBin1 = createInventoryHelper(TEST_SKU, 4, BIN_02)

    val future = InventorySingleRecordDao.findBySku(database, TEST_SKU)
    val result: Seq[InventorySingleRecord] = Await.result(future, Duration.Inf)

    result should equal(List(createdBin0, createdBin1))
  }

  test("create") {
    val future = InventorySingleRecordDao.create(database, TEST_SKU, 1, BIN_01)
    val result: Option[InventorySingleRecord] = Await.result(future, Duration.Inf)
    result should equal(Some(InventorySingleRecord(Some(1), TEST_SKU, 1, BIN_01)))

    // Validate that changes were persisted
    val inventoryTable = TableQuery[InventorySingleRecords]
    val futureFind = database.run(inventoryTable.result)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)
    findResult should contain only InventorySingleRecord(Some(1), TEST_SKU, 1, BIN_01)
  }

  test("create with update") {
    val future = InventorySingleRecordDao.create(database, TEST_SKU, 1, BIN_01)
    Await.result(future, Duration.Inf)

    val futureUpdate = InventorySingleRecordDao.create(database, TEST_SKU, 3, BIN_01)
    val resultUpdate: Option[InventorySingleRecord] = Await.result(futureUpdate, Duration.Inf)
    resultUpdate should equal(Some(InventorySingleRecord(Some(1), TEST_SKU, 3, BIN_01)))

    // Validate that changes were persisted
    val inventoryTable = TableQuery[InventorySingleRecords]
    val futureFind = database.run(inventoryTable.result)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)
    findResult should contain only InventorySingleRecord(Some(1), TEST_SKU, 3, BIN_01)
  }

//  test("create when unique constraint violated") {
//    Await.result(
//      InventorySingleRecordDao.create(database, TEST_SKU, 1, BIN_01),
//      Duration.Inf
//    )
//
//    assertThrows[PSQLException] {
//      Await.result(
//        InventorySingleRecordDao.create(database, TEST_SKU, 1, BIN_01),
//        Duration.Inf
//      )
//    }
//  }

  def createInventoryHelper(sku: String, qty: Int, location: String): InventorySingleRecord = {
    val create = InventorySingleRecordDao.create(database, sku, qty, location)
    Await.result(create, Duration.Inf).get
  }

  test("transfer") {
    createInventoryHelper(TEST_SKU, 1, BIN_01)
    createInventoryHelper(TEST_SKU, 0, BIN_02)

    val futureTrans = InventorySingleRecordDao.transfer(database, TEST_SKU, 1, BIN_01, BIN_02)
    Await.result(futureTrans, Duration.Inf)

    val futureFind = InventorySingleRecordDao.findAll(database)
    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)

    findResult should contain only (
      InventorySingleRecord(Some(1), "NewSku", 0, BIN_01),
      InventorySingleRecord(Some(2), "NewSku", 1, BIN_02),
    )
  }
//
//  test("transfer fail for insufficient funds") {
//    createInventoryHelper(TEST_SKU, BIN_01)
//
//    val futureTrans = InventorySingleRecordDao.transfer(database, TEST_SKU, 2, BIN_01, BIN_02)
//    val error = assertThrows[Exception](Await.result(futureTrans, Duration.Inf))
//
//    val futureFind = InventorySingleRecordDao.findBySku(database, TEST_SKU)
//    val findResult: Seq[InventorySingleRecord] = Await.result(futureFind, Duration.Inf)
//
//    findResult should contain only InventorySingleRecord(Some(1), TEST_SKU, 1, BIN_01)
//  }
}