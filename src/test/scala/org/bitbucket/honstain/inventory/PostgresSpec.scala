package org.bitbucket.honstain

import org.scalatest.{BeforeAndAfterAll, Suite}

import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._
import scala.concurrent.Await
import scala.concurrent.duration.Duration

trait PostgresSpec extends Suite with BeforeAndAfterAll {

  private val dbName = getClass.getSimpleName.toLowerCase
  private val driver = "org.postgresql.Driver"

  private val postgres = Database.forURL("jdbc:postgresql://localhost:5432/?user=toyinventory&password=dev", driver = driver)

  def dropDB: DBIO[Int] = sqlu"DROP DATABASE IF EXISTS #$dbName"
  def createDB: DBIO[Int] = sqlu"CREATE DATABASE #$dbName"

  override def beforeAll(): Unit = {
    super.beforeAll()
    Await.result(postgres.run(dropDB), Duration.Inf)
    Await.result(postgres.run(createDB), Duration.Inf)
  }

  override def afterAll() {
    super.afterAll()
    Await.result(postgres.run(dropDB), Duration.Inf)
  }

  val database = Database.forURL(s"jdbc:postgresql://localhost:5432/$dbName?user=toyinventory&password=dev", driver = driver)
}
