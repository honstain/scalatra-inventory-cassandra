package org.bitbucket.honstain.inventory.app

import org.bitbucket.honstain.PostgresSpec
import org.json4s.DefaultFormats
import org.scalatra.test.scalatest._
import org.json4s.jackson.Serialization.write
import org.scalatest.BeforeAndAfter
import slick.dbio.DBIO
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration


class ToyInventoryTests extends ScalatraFunSuite with BeforeAndAfter with PostgresSpec {

  implicit val formats = DefaultFormats

  addServlet(new ToyInventory(database), "/*")

  def createInventoryTable: DBIO[Int] =
    sqlu"""
        CREATE TABLE inventory_single
        (
          id bigserial NOT NULL,
          sku text,
          qty integer,
          location text,
          CONSTRAINT pk_single PRIMARY KEY (id),
          UNIQUE (sku, location)
        );
        CREATE TABLE inventory_double
        (
          id bigserial NOT NULL,
          sku text,
          qty integer,
          type text,
          location text,
          CONSTRAINT pk_double PRIMARY KEY (id)
        );
        CREATE TABLE inventory_lock
        (
          location text,
          sku text,
          revision integer,
          CONSTRAINT pk_lock PRIMARY KEY (location, sku)
        );
      """
  def dropInventoryTable: DBIO[Int] =
    sqlu"""
          DROP TABLE IF EXISTS inventory_single;
          DROP TABLE IF EXISTS inventory_double;
          DROP TABLE IF EXISTS inventory_lock;
      """

  before {
    Await.result(database.run(createInventoryTable), Duration.Inf)
  }

  after {
    Await.result(database.run(dropInventoryTable), Duration.Inf)
  }

  val TEST_SKU = "NewSku"
  val BIN_01 = "Bin-01"
  val BIN_02 = "Bin-02"

  /** *********************************************************************************
    * The tests endpoints that use the single entry accounting database design
    * *********************************************************************************
    */
  test("GET /single for no inventory") {
    get("/single") {
      status should equal (200)
      body should equal (write(List()))
    }
  }

  test("GET /single with a single item of inventory") {
    post("/single", write(Inventory(TEST_SKU, 3, BIN_01))) {}
    get("/single") {
      status should equal (200)
      body should equal (write(List(Inventory(TEST_SKU, 3, BIN_01))))
    }
  }

  test("POST /single to create/adjust inventory") {
    post("/single", write(Inventory(TEST_SKU, 3, BIN_01))) {
      status should equal (200)
    }
  }

  test("POST /single/transfer to move inventory") {
    post("/single", write(Inventory(TEST_SKU, 3, BIN_01))) {}
    post("/single", write(Inventory(TEST_SKU, 1, BIN_02))) {}
    post("/single/transfer", write(InventoryTransfer(TEST_SKU, 3, BIN_01, BIN_02))) {
      status should equal (200)
      get("/single") {
        status should equal (200)
        body should equal (write(List(
          Inventory(TEST_SKU, 4, BIN_02),
          Inventory(TEST_SKU, 0, BIN_01),
        )))
      }
    }
  }

  /** *********************************************************************************
    * The tests endpoints that use the double entry accounting database design
    * *********************************************************************************
    */
  test("GET /double for no inventory") {
    get("/double") {
      status should equal (200)
      body should equal (write(List()))
    }
  }

  test("GET /double with a single record of inventory") {
    post("/double", write(Inventory(TEST_SKU, 3, BIN_01))) {}
    get("/double") {
      status should equal (200)
      body should equal (write(List(Inventory(TEST_SKU, 3, BIN_01))))
    }
  }

  test("POST /double to create/adjust inventory") {
    post("/double", write(Inventory(TEST_SKU, 3, BIN_01))) {
      status should equal (200)
    }
  }

  test("POST /double/transfer to move inventory") {
    post("/double", write(Inventory(TEST_SKU, 3, BIN_01))) {}
    post("/double", write(Inventory(TEST_SKU, 1, BIN_02))) {}
    post("/double/transfer", write(InventoryTransfer(TEST_SKU, 3, BIN_01, BIN_02))) {
      status should equal (200)
      get("/double") {
        status should equal (200)
        body should equal (write(List(
          Inventory(TEST_SKU, 4, BIN_02),
          Inventory(TEST_SKU, 0, BIN_01),
        )))
      }
    }
  }
}