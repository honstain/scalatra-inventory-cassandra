package org.bitbucket.honstain.inventory.dao

import org.slf4j.{Logger, LoggerFactory}
import slick.jdbc.{PostgresProfile, TransactionIsolation}
import slick.jdbc.PostgresProfile.api._

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

object TRANSACTION {
  val ADJUST = "adjust"
  val TRANSFER = "transfer"
}

class InventoryDoubleRecordLocks(tag: Tag) extends Table[(String, String, Int)](tag, "inventory_lock") {
  def location = column[String]("location")
  def sku = column[String]("sku")
  def revision = column[Int]("revision")
  def * = (location, sku, revision)
}

case class InventoryDoubleRecord(
                                  id: Option[Int],
                                  sku: String,
                                  qty: Int,
                                  txnType: String,
                                  location: String
                                )

class InventoryDoubleRecords(tag: Tag) extends Table[InventoryDoubleRecord](tag, "inventory_double") {
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def sku = column[String]("sku")
  def qty = column[Int]("qty")
  def txnType = column[String]("type")
  def location = column[String]("location")
  def * =
    (id.?, sku, qty, txnType, location) <> (InventoryDoubleRecord.tupled, InventoryDoubleRecord.unapply)
}

object InventoryDoubleRecordDao extends TableQuery(new InventoryDoubleRecords(_)) {

  val logger: Logger = LoggerFactory.getLogger(getClass)

  def findAllRaw(db: PostgresProfile.backend.DatabaseDef): Future[Seq[InventoryDoubleRecord]] = {
    db.run(this.result)
  }

  def findAll(db: PostgresProfile.backend.DatabaseDef): Future[Seq[(String, String, Option[Int])]] = {
    val groupByQuery = this.groupBy(x => (x.sku, x.location))
      .map{ case ((sku, location), group) => (sku, location, group.map(_.qty).sum) }
      .result
    db.run(groupByQuery)
  }

  def findBySkuRaw(db: PostgresProfile.backend.DatabaseDef, sku: String): Future[Seq[InventoryDoubleRecord]] = {
    val findAction = this.filter(_.sku === sku)
    db.run(findAction.result)
  }

  def create(db: PostgresProfile.backend.DatabaseDef,
             location: String,
             sku: String,
             qty: Int): Future[InventoryDoubleRecord] = {
    val insert = for {
      createLock <- {
        TableQuery[InventoryDoubleRecordLocks].filter(x => x.location === location && x.sku === sku).forUpdate.result
      }

      _ <- {
        createLock match {
          case Seq((`location`, `sku`, _)) =>
            val updateLock = TableQuery[InventoryDoubleRecordLocks]
            val q = for { x <- updateLock if x.location === location && x.sku === sku } yield x.revision
            q.update(createLock.head._3 + 1)
          case _ =>
            // Create if no record lock existed
            TableQuery[InventoryDoubleRecordLocks] += (location, sku, 0)
        }
      }

      // Find the current count for this location and SKU
      initGroupBy <- this.filter(_.sku === sku).filter(_.location === location)
        .groupBy(x => x.sku).map{ case (_, group) => (sku, group.map(_.qty).sum) }
        .result.headOption

      // Insert a new record that will result in the desired count
      _ <- {
        initGroupBy match {
          case None =>
            this += InventoryDoubleRecord(Some(0), sku, qty, TRANSACTION.ADJUST, location)
          case Some((_, Some(existingQty))) =>
            logger.debug(s"INSERT need qty: $qty existing: $existingQty for sku: $sku location: $location")
            this += InventoryDoubleRecord(Some(0), sku, qty - existingQty, TRANSACTION.ADJUST, location)
          case _ => DBIO.failed(new Exception("Insert for create failed"))
        }
      }

      // Return the updated value
      newRecordGroupBy <- this.filter(_.sku === sku).filter(_.location === location)
        .groupBy(x => x.sku).map{ case (_, group) => (sku, group.map(_.qty).sum) }
        .result.headOption
      newRecordBySku <- {
        newRecordGroupBy match {
//          case Some((_, Some(newQty))) if newQty < 0 =>
//            DBIO.failed(new Exception(s"Cannot have negative inventory $newQty"))
          case Some((_, Some(newQty))) =>
            DBIO.successful (InventoryDoubleRecord (None, sku, newQty, TRANSACTION.ADJUST, location) )
          case _ =>
            DBIO.failed(new Exception("Insert for create failed"))
        }
      }
    } yield newRecordBySku

    db.run(insert.transactionally.withTransactionIsolation(TransactionIsolation.ReadCommitted))
  }

  def findQty(sku: String, location: String): Rep[Option[Int]] = {
    this.filter(x => x.sku === sku && x.location === location).map(_.qty).sum
  }

  def transfer(db: PostgresProfile.backend.DatabaseDef,
               sku: String,
               qty: Int,
               fromLocation: String,
               toLocation: String
              ): Future[Int] = {
    // This example helped me pull this together http://queirozf.com/entries/slick-3-reference-and-examples
    val insert = for {
      lockRecord <- {
        val locks = TableQuery[InventoryDoubleRecordLocks]
        locks.filter(x => x.location === fromLocation && x.sku === sku).forUpdate.result
      }

      createOrUpdate <- {
        lockRecord match {
          case Seq((`fromLocation`, `sku`, _)) =>
            // Update
            val updateFoo = TableQuery[InventoryDoubleRecordLocks]
            val q = for { x <- updateFoo if x.location === fromLocation && x.sku === sku } yield x.revision
            q.update(lockRecord.head._3 + 1)
          case _ =>
            // Create if no record lock existed
            TableQuery[InventoryDoubleRecordLocks] += (fromLocation, sku, 0)
        }
      }

      fromRecord <- findQty(sku, fromLocation).result

      _ <- if (fromRecord.get < 0) {
        logger.error(s"Invalid State - cannot transfer location:$fromLocation had:${fromRecord.get}")
        DBIO.failed(new Exception("FAILED"))
      }
      else if (fromRecord.get < qty) {
        logger.debug(s"Insufficient inventory to transfer location:$fromLocation had:${fromRecord.get} but we needed:$qty")
        DBIO.failed(new Exception("FAILED"))
      }
      else {
        logger.debug(s"Found enough inventory to transfer, location:$fromLocation had:${fromRecord.get} and we need:$qty")
        DBIO.successful(())
      }

      _ <- this += InventoryDoubleRecord(Some(0), sku, qty * -1, TRANSACTION.TRANSFER, fromLocation)
      newId <- this += InventoryDoubleRecord(Some(0), sku, qty, TRANSACTION.TRANSFER, toLocation)
    } yield newId

    // What should I return from this? both of the new records? or should I return nothing? A list of futures?
    db.run(insert.transactionally.withTransactionIsolation(TransactionIsolation.ReadCommitted))
  }
}