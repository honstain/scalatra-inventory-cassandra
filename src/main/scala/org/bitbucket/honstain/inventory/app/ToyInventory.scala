package org.bitbucket.honstain.inventory.app

import org.bitbucket.honstain.inventory.dao._
import org.scalatra._
import org.slf4j.{Logger, LoggerFactory}

import scala.util.{Failure, Success}
// JSON-related libraries
import org.json4s.{DefaultFormats, Formats}
// JSON handling support from Scalatra
import org.scalatra.json._

import slick.jdbc.PostgresProfile
import com.datastax.driver.core.SocketOptions
import com.outworkers.phantom.dsl._
import com.outworkers.phantom.builder.query.InsertQuery

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global


object ConnectorExample {
  val default: CassandraConnection = ContactPoint.apply("172.17.0.2", 9042) //.local
    .withClusterBuilder(_.withSocketOptions(
    new SocketOptions()
      .setConnectTimeoutMillis(20000)
      .setReadTimeoutMillis(20000)
  )
  ).noHeartbeat().keySpace(
    KeySpace("mykeyspace").ifNotExists().`with`(
      replication eqs SimpleStrategy.replication_factor(1)
    )
  )
}

case class InventoryRecord(
                     location: String,
                     id: UUID,
                     sku: String,
                     qty: Int
                   )

abstract class InventoryRecordEntries extends Table[InventoryRecordEntries, InventoryRecord] {

  override def tableName: String = "inventory"

  object location extends StringColumn with PartitionKey
  object id extends UUIDColumn
  object sku extends StringColumn
  object qty extends IntColumn

  def getByKey(location: String): Future[List[InventoryRecord]] = {
    select.where(_.location eqs location).fetch()
  }

  def testInsert(record: InventoryRecord): Future[ResultSet] = {
    insert
      .value(_.location, record.location)
      .value(_.id, record.id)
      .value(_.sku, record.sku)
      .value(_.qty, record.qty)
      .future()
  }
}

class BasicDatabase(override val connector: CassandraConnection) extends Database[BasicDatabase](connector) {
  object entries extends InventoryRecordEntries with Connector
}
object db extends BasicDatabase(ConnectorExample.default)

class ToyInventory(database: PostgresProfile.backend.DatabaseDef) extends ScalatraServlet with JacksonJsonSupport {

  val logger: Logger = LoggerFactory.getLogger(getClass)
  protected implicit val jsonFormats: Formats = DefaultFormats

  val doubleDAO = InventoryDoubleRecordDao
  val singleDAO = InventorySingleRecordDao

  before() {
    contentType = formats("json")
  }

  get("/cassandra/:location") {
    val foo: Future[List[InventoryRecord]] = db.entries.getByKey(params("location"))
    val result: List[InventoryRecord] = Await.result(foo, Duration.Inf)
    logger.debug(s"GET retrieved ${result.size} inventory records")

    val bar = result.groupBy(x => (x.location, x.sku)).mapValues(_.map(_.qty).sum)
    logger.debug(s"Test: ${bar}")

    Ok(bar.map{ case (key, value) => Inventory(key._2, value, key._1)})
  }

  /**
    * Inventory that lets you modify quantity of a SKU in a location.
    * BODY: Inventory
    *   The quantity with be added to the current value.
    *
    * Example: Current quantity of 'LOC-01' 'SKU-01' is 10.
    *          POST Inventory('SKU-01', 4, 'LOC-01')
    *          RESULT is for 'LOC-01' 'SKU-01' is 14.
    */
  post("/cassandra") {
    val newInventory = parsedBody.extract[Inventory]
    logger.debug(s"Creating inventory sku:${newInventory.sku} in location:${newInventory.location}")
    val id = java.util.UUID.randomUUID
    logger.debug(s"new id:$id")

    val foo = db.entries.testInsert(
      InventoryRecord(newInventory.location, id, newInventory.sku, newInventory.qty)
    )
    val result = Await.result(foo, Duration.Inf)
    logger.debug(s"Create retrieved ${result} inventory records")
    // TODO - I don't have anything meaningful to return at this time
    // Ok(result.map(x => Inventory(x.key, x.value, x.location)))
    Ok()
  }

  post("/cassandra/transfer") {
    val transfer = parsedBody.extract[InventoryTransfer]
    logger.debug(s"Transfer sku:${transfer.sku} from:${transfer.fromLocation} to:${transfer.toLocation}")

    val fromRecord = InventoryRecord(transfer.fromLocation, java.util.UUID.randomUUID, transfer.sku, -transfer.qty)
    val toRecord = InventoryRecord(transfer.toLocation, java.util.UUID.randomUUID, transfer.sku, transfer.qty)

    val multipleInserts = for {
       _ <- db.entries.testInsert(fromRecord)
       result <- db.entries.testInsert(toRecord)
    } yield result

    Await.result(multipleInserts, Duration.Inf)
    Ok(Map())
  }

  get("/single") {
    val futureResult = Await.result(singleDAO.findAll(database), Duration.Inf)
    logger.debug(s"GET retrieved ${futureResult.size} inventory records")
    Ok(futureResult.map(x => Inventory(x.sku, x.qty, x.location)))
  }

  get("/double") {
    val futureResult = Await.result(doubleDAO.findAll(database), Duration.Inf)
    //logger.debug(s"GET retrieved ${futureResult.size} inventory records")
    val head = futureResult.headOption
    head match {
      case Some((sku, loc, qty)) =>
        logger.debug(s"GET: location: $loc sku: $sku qty: $qty")
      case _ => None
    }
    Ok(futureResult.map { case (sku, loc, qty) => Inventory(sku, qty.getOrElse(0), loc) })
  }

  post("/single") {
    val newInventory = parsedBody.extract[Inventory]
    logger.debug(s"Creating inventory sku:${newInventory.sku} in location:${newInventory.location}")

    val future: Future[Option[InventorySingleRecord]] = singleDAO.create(database, newInventory.sku, newInventory.qty, newInventory.location)
    future.onComplete {
      case Success(Some(value)) => logger.debug(s"Created new inventory record id:${value.id}")
      case Success(None) =>
        // Does it even make sense to try and account case like this?
        logger.error(s"Failed to create or update sku:${newInventory.sku} in location:${newInventory.location}")
      case Failure(t) => logger.error(t.getMessage)
    }
    val result = Await.result(future, Duration.Inf)
    if (result.isDefined) {
      logger.debug(s"Created new inventory record id:${result.get}")
      // TODO - decide on more appropriate response data
      Ok
    }
    else {
      InternalServerError
    }
  }

  post("/double") {
    val newInventory = parsedBody.extract[Inventory]
    val result: Future[InventoryDoubleRecord] = doubleDAO.create(database, newInventory.location, newInventory.sku, newInventory.qty)
    result.onComplete {
      case Success(_) => None //logger.debug("Transfer transaction success")
      case Failure(t) => logger.debug("FAIL " + t.getMessage)
    }
    val futureResult = Await.result(result, Duration.Inf)

    Ok(Inventory(futureResult.sku, futureResult.qty, futureResult.location))
  }

  post("/single/transfer") {
    val transfer = parsedBody.extract[InventoryTransfer]
    logger.debug(s"Transfer sku:${transfer.sku} from:${transfer.fromLocation} to:${transfer.toLocation}")

    val future: Future[Int] = singleDAO.transfer(database, transfer.sku, transfer.qty, transfer.fromLocation, transfer.toLocation)
    future.onComplete {
      case Success(_) => logger.debug("Transfer transaction success")
      case Failure(t) => logger.debug(t.getMessage)
    }
    Await.result(future, Duration.Inf)
    Ok
  }

  post("/double/transfer") {
    val transfer = parsedBody.extract[InventoryTransfer]
    logger.debug(s"Transfer sku:${transfer.sku} from:${transfer.fromLocation} to:${transfer.toLocation}")

    val future: Future[Int] = doubleDAO.transfer(database, transfer.sku, transfer.qty, transfer.fromLocation, transfer.toLocation)
    future.onComplete {
      case Success(_) => logger.debug("Transfer transaction success")
      case Failure(t) => logger.debug(t.getMessage)
    }
    Await.result(future, Duration.Inf)
    Ok
  }
}

case class Inventory(sku: String, qty: Int, location: String)
case class InventoryTransfer(sku: String, qty: Int, fromLocation: String, toLocation: String)