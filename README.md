# Scalatra-Double-Record-Inventory #

This is the final service created as part of my series on using Scala to make a prototype inventory service.
* This expanded to also experiment with Cassandra (nothing glamorous, it is just shoe horned into the service).

This is an experimental Scala service meant to demonstrate a system for modeling supply chain inventory records. Mapping physical locations to goods (referenced as a SKU) with a quantity.

This was created as part of the following series of blog posts I created:

* Part 1 - [Creating a Scalatra Inventory Management Service](http://honstain.com/scalatra-inventory-management-service/)
* Part 2 - [Implementing Create/Update in Slick](http://honstain.com/slick-upsert-and-select/)
* Part 3 - [Inventory Management Transfer](http://honstain.com/inventory-management-transfer-start/)
* Part 4 - [Inventory Management Transfer with Row Level Locking](http://honstain.com/inventory-transfer-row-locking/)
* Part 5 - [Inventory Management Double-Entry Schema](http://honstain.com/scalatra-and-slick-for-double/)

It is a microservice to model how one might theoretically track physical inventory.

## Dependencies ##

* Scala https://www.scala-lang.org/
* Scalatra - a Scala application framework http://scalatra.org/getting-started/first-project.html
* SBT Scala Build Tool https://www.scala-sbt.org/
* PostgreSQL 10.6 `PostgreSQL 10.6 (Ubuntu 10.6-0ubuntu0.18.10.1) on x86_64-pc-linux-gnu` installed locally on Ubuntu 18.10 using sudo apt install postgresql https://www.postgresql.org/

## Build & Run ##

NOTE - You will need to have set up a PostgreSQL database - this guide may help you http://honstain.com/scalatra-2-6-4-postgresql/

```sh
export POSTGRES_DATABASE_URL="jdbc:postgresql://localhost:5432/toyinventory?user=toyinventory&password=<REPLACE-WITH-PASSWORD>"
$ sbt
> ~;jetty:stop;jetty:start
> browse
```

If `browse` doesn't launch your browser, manually open [http://localhost:8080/](http://localhost:8080/) in your browser.

For hot reload you can use the following: `~;jetty:stop;jetty:start`

The basic REST endpoints supported are:

* Endpoints using a single record database design
    * GET localhost:8080/single - return the current inventory sku,location,qty data
    * POST localhost:8080/single - adjust inventory `{"sku": "SKU-01","qty": 4,"location": "LOC-01"}`
    * POST localhost:8080/single/transfer - transfer inventory `{"sku": "SKU-01","qty": 4,"fromLocation": "LOC-01","toLocation": "LOC-02"}`
* Endpoints using a double record accounting database design
    * GET localhost:8080/double - return the current inventory sku,location,qty data
    * POST localhost:8080/double - adjust inventory `{"sku": "SKU-01","qty": 4,"location": "LOC-01"}`
    * POST localhost:8080/double/transfer - transfer inventory `{"sku": "SKU-01","qty": 4,"fromLocation": "LOC-01","toLocation": "LOC-02"}`


### Tests
The tests are very slow, because it will attempt to create a new database for each test.
Each database is created with a name representing the test class being run.
NOTE - you will need an environment variable POSTGRES_DATABASE_URL
```sh
$ export POSTGRES_DATABASE_URL="jdbc:postgresql://localhost:5432/toyinventory?user=toyinventory&password=<REPLACE-WITH-PASSWORD>"
$ sbt test
```

## Running Siege to Stress the Server

```
siege -v -c2  --content-type "application/json" -f siege_urls_cassandra.txt
siege -v -c2  --content-type "application/json" -f siege_urls_single.txt
siege -v -c2  --content-type "application/json" -f siege_urls_double.txt
```

## Helpful SQL

```sql
CREATE TABLE inventory_double
(
  id bigserial NOT NULL,
  sku text,
  qty integer,
  type text,
  location text,
  CONSTRAINT pk_double PRIMARY KEY (id)
);

CREATE TABLE inventory_lock
(
  location text,
  sku text,
  revision integer,
  CONSTRAINT pk_lock PRIMARY KEY (location, sku)
);

INSERT INTO inventory_double(sku, qty, type, location) VALUES
('SKU-01', 2, 'adjust', 'LOC-01'),
('SKU-01', 0, 'adjust', 'LOC-02')
;

CREATE TABLE inventory_single
(
  id bigserial NOT NULL,
  sku text,
  qty integer,
  location text,
  CONSTRAINT pk_single PRIMARY KEY (id),
  UNIQUE (sku, location)
);

INSERT INTO inventory_single(sku, qty, location) VALUES
('SKU-01', 2, 'LOC-01'),
('SKU-01', 0, 'LOC-02')
;
```

Cassandra Queries
```sql
CREATE KEYSPACE mykeyspace
    WITH replication = {
    'class' : 'NetworkTopologyStrategy',
    'datacenter1' : 1,
    'datacenter2' : 1
    };

ALTER KEYSPACE mykeyspace
    WITH REPLICATION = {
    'class' : 'NetworkTopologyStrategy',
    'datacenter1' : 2,
    'datacenter2' : 1
    };

CREATE TABLE inventory(location text primary key,
                       sku text,
                       qty int );

insert into inventory (location, sku, qty) values ('LOC-01', 'SKU-01', 5);

SELECT * FROM inventory;

CREATE TABLE inventory(
                          location text,
                          id UUID,

                          sku text,
                          qty int,
                          PRIMARY KEY (location, id)
);

insert into inventory (location, id, sku, qty) values ('LOC-01', UUID(), 'SKU-01', 5);
insert into inventory (location, id, sku, qty) values ('LOC-01', 546f1c05-4f46-4b1c-987c-4d113ce06afb, 'SKU-01', 5);
insert into inventory (location, id, sku, qty) values ('LOC-01', UUID(), 'SKU-02', 2);

SELECT COUNT(*) FROM inventory WHERE location = 'LOC-01';
SELECT * FROM inventory WHERE location = 'LOC-01';
SELECT * FROM inventory WHERE location = 'anthony-pack';
SELECT * FROM inventory WHERE location = 'location-032330';

SELECT SUM(qty) FROM inventory WHERE location = 'LOC-01';
```

